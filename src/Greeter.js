myapp = {};

myapp.Greeter = function() { };

myapp.Greeter.prototype.greet = function(name) {
    if(name == undefined || name == null)
        return null
    else
        return "Hello " + name + "!";
};
