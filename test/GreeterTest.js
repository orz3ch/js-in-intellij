test( "first", function() {
        var greeter = new myapp.Greeter();
        assertNull(greeter.greet(null));
});

test( "second", function() {
        var greeter = new myapp.Greeter();
        assertEquals("Hello Orzech!", greeter.greet("Orzech"));
});
